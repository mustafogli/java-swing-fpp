package com.mustafogli;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class MyPanel implements ActionListener {

    private static JLabel inLabel, outLabel;
    private static JTextField input, output;
    private static JButton buttonCount, buttonReverse, buttonRemove;

    MyPanel(){}

    public static void main(String[] args) {
        JPanel panel = new JPanel();
        JFrame frame = new JFrame();
        frame.setTitle("String Utility");
        frame.setSize(500,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(panel);

        panel.setLayout(null);

        inLabel = new JLabel("Input");
        inLabel.setBounds(290,20,80,25);
        panel.add(inLabel);

        input = new JTextField(30);
        input.setBounds(280,40,165,25);
        panel.add(input);

        outLabel = new JLabel("Output");
        outLabel.setBounds(290,60,80,25);
        panel.add(outLabel);

        output = new JTextField(30);
        output.setBounds(280,80,165,25);
        panel.add(output);

        buttonCount = new JButton("Count Letters");
        buttonCount.setBounds(60,20,180,30);
        buttonCount.addActionListener(new MyPanel());
        panel.add(buttonCount);

        buttonReverse = new JButton("Reverse Letters");
        buttonReverse.setBounds(60,50,180,30);
        buttonReverse.addActionListener(new MyPanel());
        panel.add(buttonReverse);

        buttonRemove = new JButton("Remove Letters");
        buttonRemove.setBounds(60,80,180,30);
        buttonRemove.addActionListener(new MyPanel());
        panel.add(buttonRemove);

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String myInput = input.getText();

        if (e.getSource() == buttonCount){
            int count = myInput.length();
            output.setText(String.valueOf(count));
        }else if (e.getSource() == buttonReverse){
            String res ="";
            char c;
            for (int i=0; i<myInput.length();i++){
                c = myInput.charAt(i);
                res = c + res;
            }
            output.setText(res);
        }else if (e.getSource() == buttonRemove){
            String newstr = new String();
            int length = myInput.length();
            for (int i = 0; i < length; i++)
            {
                char charAtPosition = myInput.charAt(i);
                if (newstr.indexOf(charAtPosition) < 0)
                {
                    newstr += charAtPosition;
                }
            }
            output.setText(newstr);
        }
    }
}
